/**********     用户表				BY  Jaysir 		2015.6.21
***********
***********					可搜索以下关键词来查看未实现功能	
***********	
***********					TODO	: 	未完成
***********					DONE	: 	已完成
***********					TOTEST  : 	待测试
***********					NOTEST  : 	无需测试
***********					WAITING : 	功能待定
***********
***********			接口：	（暂未实现 增加分组，更改好友分组等功能）
***********							findOneByEmail 	(email ,callback)	;
***********							addFriend 		(myEmail,otherEmail,callback);
***********							delFriend 		(myEmail,otherEmail,callback);
***********							getFriendList	(email , callback);
***********							regInitData		(options , callback);
***********							updateInf		(options , callback);
***********/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// 定义用户表结构
var UserSchema = new Schema({
	user_email		:	{type : String , required : true},
	user_password	:	{type : String , required : true},
	user_nickname	:	{type : String , required : true},
	user_sex		:	{type : String , required : true,enum:['男','女']},
	user_pic		:	{type : String , required : true},
	
	user_friend_groups:[
			{
				g_name	: String,
				g_sort	: Number,
				list	: [
					{
						friend : {type:Schema.Types.ObjectId,ref:'users'},		//	引用users表   查询时可以解引用即为用户的好友列表
						remark : {type:String}
					}
				]
			}
		],
	user_reg_time	:	{type : Date , required : false}
});

var userModel = mongoose.model('users', UserSchema);    	//

//		DONE       NOTEST
UserSchema.statics.findOneByEmail = function(email, callback) {
	this.findOne({user_email:email}, callback);
};


//添加好友	DONE,		TOTEST		//查询代码(以下几行注释为命令行下测试代码)mongo命令行下已测试
// db.users.update({user_email:"jaysir@163.com","user_friend_groups.g_sort":1},	//user_email,g_sort需存在
// 				  {"$push": {
// 				  			"user_friend_groups.$.list":{ 
// 				  										friend  :  {
// 				  													"$ref" : "users",
// 				  													"$id"  : ObjectId("55867dd74389afa216e60fad")
// 				  										}, 
// 				  										remark  :  "呵呵1" 
// 				  			}
// 				  }});
UserSchema.statics.addFriend = function(myEmail,otherEmail,callback){
	var myId , otherId;
	var self = this;
	self.findOneByEmail(myEmail,function(err,doc){
		myId = doc._id;
		self.findOneByEmail(otherEmail,function(err,doc){
			otherId = doc._id;
			if(!otherId){callback("不存在的用户！");return;}
			//添加好友都默认添加到  “我的好友”  列表  
			self.update({_id:myId},{"$push":{"user_friend_groups.0.list":{friend:otherId,remark:""}}},callback(err));
			self.update({_id:otherId},{"$push":{"user_friend_groups.0.list":{friend:myId,remark:""}}},callback(err));
		});
	});
	// // 介于js异步执行，这里将添加好友放至 取得myId 与 otherId 后的回调里嵌套执行。防止未取得Id值就执行添加好友到列表而错误
}
//删除好友  DONE,		TOTEST		//查询代码(以下几行注释为命令行下测试代码)mongo命令行下已测试(同addFriend代码一起测试)
// db.users.update({	user_email:"jaysir@163.com",
// 				 	"user_friend_groups.list.remark":"呵呵1"
// 				},
// 				{"$pull":{
// 							"user_friend_groups.$.list":{remark:"呵呵1"}
// 						}
// 				})
UserSchema.statics.delFriend = function(myEmail,otherEmail,callback){
	var myId , otherId;
	var myId , otherId;
	var self = this;
	self.findOneByEmail(myEmail,function(err,doc){
		myId = doc._id;
		self.findOneByEmail(otherEmail,function(err,doc){
			otherId = doc._id;
			if(!otherId){callback("不存在的用户！");return;}
			self.update({_id:myId,"user_friend_groups.list.friend":otherId},{"$pull":{"user_friend_groups.$.list":{friend:otherId}}},callback(err));
			self.update({_id:otherId,"user_friend_groups.list.friend":myId},{"$pull":{"user_friend_groups.$.list":{friend:myId}}},callback(err));
		});
	});
}
//用户登陆后 获取初始化的数据		DONE  TOTEST		
//此处获取用户好友列表并解引用可以直接  result.user_friend_groups[0].list[0].friend.user_email,访问用户好友的Email等信息
//用户信息安全考虑，屏蔽掉解引用后好友的部分信息（未屏蔽，实现方案待定 WAITING）
UserSchema.statics.getFriendList = function(email , callback){
	var self = this;
	self.findOne({user_email:email}).populate("user_friend_groups.list.friend").exec().then(function(result){
		//对用户好友列表进行排序 
		result.user_friend_groups.sort(function(obj1,obj2){return obj1.g_sort-obj2.g_sort;});
		callback(result);
	});
}
//用户注册 初始化数据		DONE  TOTEST

UserSchema.statics.regInitData = function(options , callback){
	var userInf = {
		user_email		: 	options.email ? options.email : options.user_email,
		user_password	: 	options.password ? options.password : options.user_password,
		user_nickname 	: 	(options.nickname||options.user_nickname) ? (options.nickname||options.user_nickname) : "",
		user_sex		: 	(options.sex == "女")||(options.user_sex == "女") ? "女" : "男",
		user_pic 		: 	"url",
		"user_friend_groups.0" : {
					g_name : "我的好友",
					g_sort : 0,
					list   : []
				},
		user_reg_time 	: 	Date()
	}
	var newUser = new userModel(userInf);
	newUser.save(callback());
}

//更新用户信息
UserSchema.statics.updateInf = function(options,callback){
	//TODO
}

module.exports = mongoose.model('users', UserSchema);