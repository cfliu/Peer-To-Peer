/**
 * Created by mcbird on 4/18/15.
 */
var express = require('express');
var router = express.Router();

var request = require('request');
var url = ['https://unsplash.com/rss', 'http://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1'];
var supportscreenResolution = ['1024x768', '1366x768', '1920x1080'];
router.get('/', function(req, res, next) {
	var screenResolution =  supportscreenResolution.indexOf(req.params.screenResolution) != -1 ? req.params.screenResolution : '1920x1080';
	request(url[1], function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var _body = JSON.parse(body);
			var imgUrl = _body['images']['0']['url'].replace(/1920x1080/img, screenResolution);
			res.send(imgUrl);
		}
	})
});

module.exports = router;